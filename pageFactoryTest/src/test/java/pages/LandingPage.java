package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPage {
    private WebDriver driver;
    @FindBy(xpath = "//img[contains(@class, 'imglogo')]")
    private WebElement title;
    @FindBy(xpath = "//div[contains(@class, 'additional_buttons_but')]")
    private WebElement enterButton;

    public LandingPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickButton() {
        enterButton.click();
        driver.switchTo().window(driver.getWindowHandles().toArray()[1].toString());
    }


}
