import core.BaseWebTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LandingPage;
import pages.LoginPage;

public class PageFactoryTest extends BaseWebTest {

    @Test
    public void testPageObject() {
        LandingPage landingPage = new LandingPage(driver);
        landingPage.clickButton();

        LoginPage loginPage = new LoginPage(driver);
        Assert.assertTrue(loginPage.getLoginButton().isDisplayed());
    }

}
