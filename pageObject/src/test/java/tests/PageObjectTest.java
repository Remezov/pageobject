package tests;

import core.BaseWebTest;
import org.testng.annotations.Test;
import pages.LandingPage;
import pages.LoginPage;

public class PageObjectTest extends BaseWebTest {
    @Test
    public void testPageObject() {
        LoginPage loginPage = new LandingPage().clickEnterButton();

        softAssert.checkIsEnabled(loginPage.getLoginButtonLocator());
        softAssert.assertAll();
    }
}
