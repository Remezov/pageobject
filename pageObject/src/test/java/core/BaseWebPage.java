package core;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;

public abstract class BaseWebPage {

    protected SelenideElement shouldBeVisible(By element) {
        return $(element).shouldBe(visible, ofSeconds(10));
    }

    protected void clickElement(By element) {
        shouldBeVisible(element).click();
    }
}
