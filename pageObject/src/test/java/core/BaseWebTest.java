package core;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.open;

public abstract class BaseWebTest {
    protected BaseSoftAssert softAssert;

    @BeforeSuite
    public void beforeSuite() {
        baseUrl = "http://welcome.technokratos.tilda.ws/";
        browser = "chrome";
        startMaximized = true;
        timeout = 10000;
        browserSize = "1028x1080";
//        headless = true;
    }

    @BeforeClass
    public void beforeClass() {
        open(baseUrl);
    }

    @BeforeMethod
    public void beforeMethod() {
        softAssert = new BaseSoftAssert();
    }
}
