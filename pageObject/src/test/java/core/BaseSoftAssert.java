package core;

import org.openqa.selenium.By;
import org.testng.asserts.SoftAssert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;

public class BaseSoftAssert {
    private SoftAssert softAssert;

    public BaseSoftAssert() {
        this.softAssert = new SoftAssert();
    }

    public void assertAll() {
        softAssert.assertAll();
    }

    public void checkIsEnabled(By element) {
        softAssert.assertTrue($(element).shouldBe(visible, ofSeconds(10)).isEnabled(),
                "Element " + element.toString() + " is disabled.");
    }
}
