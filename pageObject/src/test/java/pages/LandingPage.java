package pages;

import core.BaseWebPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.switchTo;

public class LandingPage extends BaseWebPage {
    private final By TITLE_LOCATOR = new By.ByXPath("//img[contains(@class, 'imglogo')]");
    private final By ENTER_BUTTON_LOCATOR = new By.ByXPath("//div[contains(@class, 'additional_buttons_but')]");

    public LandingPage() {
        shouldBeVisible(TITLE_LOCATOR);
    }

    public LoginPage clickEnterButton() {
        clickElement(ENTER_BUTTON_LOCATOR);
        switchTo().window(1);
        return new LoginPage();
    }
}
