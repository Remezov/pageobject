package pages;

import core.BaseWebPage;
import org.openqa.selenium.By;

public class LoginPage extends BaseWebPage {
    private final By TITLE_LOCATOR = new By.ByXPath("//h1[text()='Технокра́тер']");
    private final By LOGIN_BUTTON_LOCATOR = new By.ByXPath("//span[text() = 'Войти с помощью Google']");

    public LoginPage() {
        shouldBeVisible(TITLE_LOCATOR);
    }

    public By getLoginButtonLocator() {
        return LOGIN_BUTTON_LOCATOR;
    }
}
